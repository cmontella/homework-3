#include <stdio.h>
#include <stdlib.h>
#include "linkedlist.h"

// Adding some code



// This is my comment
// This is my other comment

void printList(List* list) {
	Node* node;

	printf("This is a test!!!!");

  // Handle an empty node. Just print a message.
	if(list->head == NULL) {
		printf("\nEmpty List");
		return;
	}
	
  // Start with the head.
	node = (Node*) list->head;

	printf("\nList: \n\n\t"); 
	while(node != NULL) {
		printf("[ %x ]", node->item);

    // Move to the next node
		node = (Node*) node->next;

		if(node !=NULL) {
			printf("-->");
    }
	}
	printf("\n\n");
}